﻿using UnityEngine;

public class Follower : MonoBehaviour {

    public GameObject Target;
    public Vector3 Offset;

    void Update() {
        Vector3 targetPosition = Target.transform.position;
        Vector3 nextPosition;
        nextPosition.x = targetPosition.x + Offset.x;
        nextPosition.y = targetPosition.y + Offset.y;
        nextPosition.z = targetPosition.z + Offset.z;

        transform.position = nextPosition;
    }

}