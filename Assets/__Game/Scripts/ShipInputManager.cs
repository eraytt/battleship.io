﻿using UnityEngine;

public class ShipInputManager : MonoBehaviour {

    [SerializeField]
    private Ship ship;

    private void FixedUpdate() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        int layerMask = 1 << 4;
        bool hasHit = Physics.Raycast(ray, out hit, 1000, layerMask);

        Rigidbody shipRigidbody = ship.GetComponentInChildren<Rigidbody>();
        if (hasHit) {
            Vector3 targetDirection = hit.point - ship.transform.position;
            Quaternion targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);
            Quaternion nextRotation   = Quaternion.Lerp(shipRigidbody.rotation, targetRotation, ship.RotateSpeed * Time.fixedDeltaTime);
            shipRigidbody.MoveRotation(nextRotation);


            float angleToTarget = Quaternion.Angle(shipRigidbody.rotation, targetRotation);
            float speedModifier = 1 - (angleToTarget / 180.0f);
            float speed = EasingFunction.EaseInSine(0.0f, ship.ForwardSpeed, speedModifier);
            Debug.LogErrorFormat("Angle: {0}, Speed: {1}", angleToTarget, speed); // TODO: remove
            shipRigidbody.MovePosition(shipRigidbody.position + ship.transform.forward * Time.fixedDeltaTime * speed);
        } else {
            Debug.LogError("No Hit!"); // TODO: you may want to remove it
        }
    }

}